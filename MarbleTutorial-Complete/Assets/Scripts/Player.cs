﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public float deathBelowHeight = -20.0f;

	/* If the player is below a certain height, we want it to be Game Over. */
	void FixedUpdate() {
		if (transform.position.y < deathBelowHeight) {
			GameObject gameController = GameObject.FindWithTag("GameController");
			gameController.GetComponent<GameController>().GameOver();
		}
	}
}
