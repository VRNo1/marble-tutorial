﻿using UnityEngine;
using System.Collections;

public class FixedShadow : MonoBehaviour {

	private Vector3 displacement;
	private Quaternion rotation;

	void Start () {
		// Save initial relative position and rotation.
		displacement = transform.localPosition;
		rotation = transform.rotation;
	}

	void FixedUpdate () {
		// Reset rotation and position to their initial values.
		transform.rotation = rotation;
		transform.position = transform.parent.position + displacement;
	}
}
