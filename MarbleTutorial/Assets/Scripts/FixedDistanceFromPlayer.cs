﻿using UnityEngine;
using System.Collections;

public class FixedDistanceFromPlayer : MonoBehaviour {

	private Transform player;
	private Vector3 displacement;

	// This happens when the object is first created and enabled.
	void Start () {
		GameObject playerObj = GameObject.FindWithTag("Player");
		// Save the player and the displacement from the player to this object.
		player = playerObj.transform;
		displacement = transform.position - player.position;
	}

	// This happens once every physics step.
	void FixedUpdate() {
		if (player == null) {
			return;
		}
		// Relocate self to preserve the displacement to the player.
		transform.position = player.position + displacement;
	}
}
